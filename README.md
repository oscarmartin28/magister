RETO MAGISTER

Aqui dejo un pequeño README con los pasos para descargar e instalar la aplicación.

INSTALACIÓN

PASO 1: En la terminal ejecutar el comando git clone git@gitlab.com:oscarmartin28/magister.git

PASO 2: En la carpeta del proyecto, ejecutar el comando npm install

PASO 3: Iniciar la aplicacion con npm run start.

ANOTACIONES

He decidido implementar redux para poder tener un state global, que es el de matrícula.

No he podido hacer la aplicación todo lo bien que me hubiera gustado debido a la falta de tiempo a si que no he podido seguir todas las buenas prácticas ni completarla totalmente.

No he cambiado el estilo al seleccionar las opciones, es decir cambiar el color de un boton seleccionado para que el usuario sepa que le ha pulsado, debido a la falta de tiempo.


