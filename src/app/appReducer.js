export const setRamaReducer = (state, action) => {
  state.matricula.rama = action.payload;
};

export const setProvinciaReducer = (state, action) => {
    state.matricula.provincia = action.payload;
};

export const setModalidadReducer = (state, action) => {
    state.matricula.modalidad = action.payload;
};

export const setHorarioReducer = (state, action) => {
  state.matricula.horario = action.payload;
};

export const setTarifaReducer = (state, action) => {
  state.matricula.tarifa = action.payload;
};

export const setComunidadReducer = (state, action) => {
  state.matricula.comunidad = action.payload;
};

export const setPersonalInfoNameReducer = (state, action) => {
  state.matricula.personalInfo.name = action.payload;
};

export const setPersonalInfoDniReducer = (state, action) => {
  state.matricula.personalInfo.dni = action.payload;
};

export const setPersonalInfoMovilReducer = (state, action) => {
  state.matricula.personalInfo.movil = action.payload;
};

export const setPersonalInfoEmailReducer = (state, action) => {
  state.matricula.personalInfo.email = action.payload;
};

export const setFormaDePagoReducer = (state, action) => {
  state.matricula.formaDePago = action.payload;
};