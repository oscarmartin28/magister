import { createSlice } from "@reduxjs/toolkit";
import {setModalidadReducer, setProvinciaReducer,
        setRamaReducer, setHorarioReducer, setTarifaReducer,
        setComunidadReducer, setPersonalInfoNameReducer, setPersonalInfoMovilReducer,
        setPersonalInfoDniReducer, setPersonalInfoEmailReducer,
        setFormaDePagoReducer} from "./appReducer";

export const appSlice = createSlice({
    name: 'app',
    initialState: {
        matricula: {
            rama: '',
            provincia: '',
            modalidad: '',
            horario: '',
            tarifa: '',
            personalInfo: {
                name: '',
                dni: '',
                movil: '',
                email: ''
            },
            comunidad: '',
            formaDePago: '',
        },

    },
    reducers: {
        setRama: setRamaReducer,
        setProvincia: setProvinciaReducer,
        setModalidad: setModalidadReducer,
        setHorario: setHorarioReducer,
        setTarifa: setTarifaReducer,
        setComunidad: setComunidadReducer,
        setPersonalInfoName: setPersonalInfoNameReducer,
        setPersonalInfoDni: setPersonalInfoDniReducer,
        setPersonalInfoMovil: setPersonalInfoMovilReducer,
        setPersonalInfoEmail: setPersonalInfoEmailReducer,
        setFormaDePago: setFormaDePagoReducer,
    }
                                    });

export const {
    setRama,
    setProvincia,
    setModalidad,
    setHorario,
    setTarifa,
    setComunidad,
    setPersonalInfoName,
    setPersonalInfoDni,
    setPersonalInfoMovil,
    setPersonalInfoEmail,
    setFormaDePago,
} = appSlice.actions;

export const selectMatricula = (state) => state.app.matricula;

export default appSlice.reducer;