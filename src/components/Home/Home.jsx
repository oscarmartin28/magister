import React from 'react';
import {Link} from 'react-router-dom';

import './Home.scss';

class Home extends React.Component {

  render() {
    return (
      <div className="home">
        <div className="home__title">
            <img className="home__img" src="./img/logo2.png" alt=""/>
            <h2 className="title">Magister</h2>
        </div>
        
        <h1 className="home__title2">¡Comencemos con tu matrícula!</h1>
        <p className="home__description">Para comenzar a especializarte, vamos a realizar unas preguntas para darte el mejor servicio.</p>

        <div className="home__button">
            <button className="button-1"><Link className="link" to="/step1">Comenzar</Link></button>
            <button className="button-2"><Link className="link" to="/">Volver atrás</Link></button>
        </div>
      </div>
    );
  }
}

export default Home;