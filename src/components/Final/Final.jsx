import React from 'react';
import {Link} from 'react-router-dom';

import './Final.scss';

class Final extends React.Component {

  render() {
    return (
      <div className="final">
        <div className="final-title">
            <img className="final__img" src="./img/logo2.png" alt=""/>
            <h2 className="final__title">Magister</h2>
        </div>
        
        <h1 className="final__title2">¡Gracias!</h1>
        <h1 className="final__title2">Tu reserva está en proceso</h1>
        <p className="final__description">La matrícula solo será efectiva una vez que se haya recibido el Pago por tarjeta o transferencia</p>

        <div className="final__button">
            <button className="button-1"><Link className="link" to="/">Volver a home</Link></button>
            <button className="button-2"><Link className="link" to="/step6">Volver atrás</Link></button>
        </div>
      </div>
    );
  }
}

export default Final;