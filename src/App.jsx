import './App.scss';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import 'firebase/firestore';
import {db} from './firebase/firebaseConfig';

//COMPONENTES
import Home from './components/Home';
import Step1 from './containers/Step1';
import Step2 from './containers/Step2';
import Step3 from './containers/Step3';
import Step4 from './containers/Step4';
import Step5 from './containers/Step5';
import Step6 from './containers/Step6';
import Final from './components/Final';

function App() {

//DEFINIMOS LOS STATE
  const [ramasList, setRamasList] = useState();
  const [provinciasList, setProvinciasList] = useState();
  const [modalidadesList, setModalidadesList] = useState();
  const [horariosList, setHorariosList] = useState();
  const [tarifasList, setTarifasList] = useState();
  const [comunidadesList, setComunidadesList] = useState();

//USO DEL USE EFFECT PARA RECOGER LOS DATOS DEL BACK Y METERLOS EN EL STATE
  useEffect(() => {
    getRamas();
    getProvincias();
    getModalidades();
    getHorarios();
    getTarifas();
    getComunidades();
  }, []);

  //FUNCIÓN PARA RECOGER DE LA BASE DE DATOS UN ARRAY CON LAS RAMAS
  const getRamas = async () => {
    let list = [];
    const response = await db.collection('ramas').get();
      response.forEach( document => {
        let maestros = document.data().maestros;
        list.push(maestros);
      })
    setRamasList(...list);
  } 
  // console.log('RAMAS', ramasList);

  //FUNCIÓN PARA RECOGER DE LA BASE DE DATOS UN ARRAY CON LAS PROVINCIAS
  const getProvincias = async () => {
    let list = [];
    const response = await db.collection('provincias').get();
      response.forEach( document => {
        let data = document.data().provincia;
        list.push(data);
      })
    setProvinciasList(...list);
  } 
  // console.log('PROVINCIAS', provinciasList);

  const getModalidades = async () => {
    let list = [];
    const response = await db.collection('modalidades').get();
      response.forEach( document => {
        let data = document.data().modalidad;
        list.push(data);
      })
    setModalidadesList(...list);
  } 
  // console.log('MODALIDADES', modalidadesList);

  const getHorarios = async () => {
    let list = [];
    const response = await db.collection('horarios').get();
      response.forEach( document => {
        let data = document.data().horario;
        list.push(data);
      })
    setHorariosList(...list);
  } 
  // console.log('HORARIOS', horariosList);

  const getTarifas = async () => {
    let list = [];
    const response = await db.collection('tarifas').get();
      response.forEach( document => {
        let data = document.data().tarifa;
        list.push(data);
      })
    setTarifasList(...list);
  } 
  // console.log('TARIFAS', tarifasList);

  const getComunidades = async () => {
    let list = [];
    const response = await db.collection('comunidades').get();
      response.forEach( document => {
        let data = document.data().comunidad;
        list.push(data);
      })
    setComunidadesList(...list);
  } 
  // console.log('COMUNIDADES', comunidadesList);

  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/final" exact component={Final} />
          <Route path="/step6" exact component={Step6} />

          <Route path="/step5" exact component={(props) => (
            <Step5 {...props} comunidadesList={comunidadesList} />
          )} />

          <Route path="/step4" exact component={Step4} />

          <Route path="/step3" exact component={(props) => (
            <Step3 {...props} tarifasList={tarifasList} />
          )} />

          <Route path="/step2" exact component={(props) => (
            <Step2 {...props} modalidadesList={modalidadesList} horariosList={horariosList} />
          )} />

          <Route path="/step1" exact component={(props) => (
            <Step1 {...props} provinciasList={provinciasList} ramasList={ramasList} />
          )} />

          <Route path="/" component={Home} />

        </Switch>
      </div>
    </Router>
  );
}

export default App;
