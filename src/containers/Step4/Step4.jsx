import React from 'react';
import { useDispatch } from 'react-redux';
import {setPersonalInfoName, setPersonalInfoDni,
        setPersonalInfoMovil, setPersonalInfoEmail} from "../../app/appSlice";

import './Step4.scss';
import {Link} from 'react-router-dom';
import Steps from '../Steps';

const Step4 = props => {

  const dispatch=useDispatch();

  const handleChangeName = (ev) => {
    dispatch(setPersonalInfoName(ev.target.value));
  };

  const handleChangeDni = (ev) => {
    dispatch(setPersonalInfoDni(ev.target.value));
  };

  const handleChangeMovil = (ev) => {
    dispatch(setPersonalInfoMovil(ev.target.value));
  };

  const handleChangeEmail = (ev) => {
    dispatch(setPersonalInfoEmail(ev.target.value));
  };


    return (
      <div className="step4">
        <Steps />
        <div className="step-4">
          <h1 className="step-4__title">Tus datos personales</h1>

          <div className="datos-personales__box1">
            <p className="datos-personales__name">Nombre completo</p>
            <form className="datos-personales__form">
                <input
                type="text"
                name="name"
                onChange={handleChangeName} 
                placeholder="Ej: Camila Perez Sanchez" 
                className="datos-personales__input"/>
            </form>
          </div>

          <div className="datos-personales__box2">
            <div className="datos-personales__box">
                <p className="datos-personales__name">DNI - NIE</p>
                <form className="datos-personales__form">
                    <input
                    type="text" 
                    name="dni"
                    onChange={handleChangeDni} 
                    placeholder="Ej: 78548568Y" 
                    className="datos-personales__input2"/>
                </form>
            </div>

            <div className="datos-personales__box">
                <p className="datos-personales__name">Móvil</p>
                <form className="datos-personales__form">
                    <input
                    type="text"
                    name="movil"
                    onChange={handleChangeMovil} 
                    placeholder="Ej: 659 34 55 42" 
                    className="datos-personales__input2"/>
                </form>
            </div>

            <div className="datos-personales__box">
                <p className="datos-personales__name">Email</p>
                <form className="datos-personales__form">
                    <input
                    type="text"
                    name="email"
                    onChange={handleChangeEmail} 
                    placeholder="Ej: cam.per.san@gmail.com" 
                    className="datos-personales__input2"/>
                </form>
            </div>
          </div>

            <div className="step-buttons">
              <button className="button-1"><Link className="link" to="/step5">Siguiente</Link></button>
              <button className="button-2"><Link className="link-two" to="/step3">Atrás</Link></button>
            </div>
        </div>
      </div>
    );
  }

export default Step4;