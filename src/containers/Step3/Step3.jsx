import React from 'react';
import { useDispatch } from 'react-redux';
import {setTarifa} from "../../app/appSlice";
import './Step3.scss';
import {Link} from 'react-router-dom';
import Steps from '../Steps';

const Step3 = props => {

  const dispatch=useDispatch();
    const { tarifasList } = props;

    console.log('props redux: ', tarifasList);

    const handleChangeTarifa = (ev) => {
        dispatch(setTarifa(ev.target.value));
    };


    return (
      <div className="step3">
        <Steps />
        <div className="step-3">
          <h1 className="step-3__title">Selecciona tu tarifa</h1>

          <div className="tarifa">
            <h3 className="tarifa__title">Tarifas</h3>
            <p className="tarifa__description">(Selecciona una opción)</p>

            { tarifasList ? (
              <div className="tarifa-buttons">

                { tarifasList.map(tarifa => {
                return(
                  <button key={tarifa} onClick={handleChangeTarifa} value={tarifa} className="tarifa__button">{tarifa}</button>
                )
                })}

              </div>
            ): null}

          </div>

            <div className="step-buttons">
              <button className="button-1"><Link className="link" to="/step4">Siguiente</Link></button>
              <button className="button-2"><Link className="link-two" to="/step2">Atrás</Link></button>
            </div>
        </div>
      </div>
    );
  }

export default Step3;