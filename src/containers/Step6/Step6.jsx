import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {setFormaDePago, selectMatricula } from "../../app/appSlice";
import './Step6.scss';
import {Link} from 'react-router-dom';
import Steps from '../Steps';

const Step6 = props => {
  
  const dispatch=useDispatch();

  const matricula = useSelector(selectMatricula);

  console.log('MATRICULA', matricula);

  const handleChangePago = (ev) => {
      dispatch(setFormaDePago(ev.target.value));
  };

    return (
      <div className="step6">
        <Steps />
        <div className="step-6">
          <h1 className="step-6__title">Forma de pago</h1>

          <div className="pago-box1">
            <h3 className="pago__title">¿Cómo prefieres abonar tu primer pago?</h3>
            <div className="pago-buttons">
                <button onClick={handleChangePago} value="Tarjeta de crédito/débito" className="pago__button">Tarjeta de crédito/débito (recomendado)</button>
                <button onClick={handleChangePago} value="Transferencia Bancaria" className="pago__button">Transferencia bancaria</button>
            </div>
            <p className="pago__description">Detalle sobre forma de pago y proceso post pago</p>
          </div>

          <div className="pago-box1 box2">
            <h3 className="pago__title">¿Vienes recomendado por alguien?</h3>
            <div className="pago-buttons">
                <button className="pago__button button2">Si</button>
                <button className="pago__button button2">No</button>
            </div>
            <p className="pago__description">Ver información legal</p>
          </div>

            <div className="step-buttons">
              <button className="button-1"><Link className="link" to="/final">Enviar</Link></button>
              <button className="button-2"><Link className="link-two" to="/step5">Atrás</Link></button>
            </div>
        </div>
      </div>
    );
  }

export default Step6;