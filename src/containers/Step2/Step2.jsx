import React from 'react';
import { useDispatch } from 'react-redux';
import {setModalidad, setHorario} from "../../app/appSlice";
import './Step2.scss';
import {Link} from 'react-router-dom';
import Steps from '../Steps';

const Step2 = props => {

  const dispatch=useDispatch();
    const { horariosList, modalidadesList } = props;

    console.log('props redux: ', horariosList, modalidadesList)

    const handleChangeHorario = (ev) => {
        dispatch(setHorario(ev.target.value));
    };

    const handleChangeModalidad = (ev) => {
        dispatch(setModalidad(ev.target.value));
    };

    return (
      <div className="step2">
        <Steps />
        <div className="step-2">
          <h1 className="step-2__title">Elige el horario y la modalidad que mas te acomode</h1>
          <div className="modalidad">
            <h3 className="modalidad__title">Modalidad</h3>
            <p className="modalidad__description">(Selecciona una opción)</p>

            { modalidadesList ? (
              <div className="modalidad-buttons">

                { modalidadesList.map(modalidad => {
                return(
                  <button key={modalidad} onClick={handleChangeModalidad} value={modalidad} className="modalidad__button">{modalidad}</button>
                )
                })}

              </div>
            ): null}
                
          </div>

          <div className="horario">
            <h3 className="horario__title">Horario</h3>
            <p className="horario__description">(Selecciona una opción)</p>

            { horariosList ? (
              <div className="horario-buttons">

                { horariosList.map(horario => {
                return(
                  <button key={horario} onClick={handleChangeHorario} value={horario} className="horario__button">{horario}</button>
                )
                })}

              </div>
            ): null}

          </div>

            <div className="step-buttons">
              <button className="button-1"><Link className="link" to="/step3">Siguiente</Link></button>
              <button className="button-2"><Link className="link-two" to="/step1">Atrás</Link></button>
            </div>
        </div>
      </div>
    );
  }

export default Step2;