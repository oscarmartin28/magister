import React from 'react';

import './Steps.scss';

class Steps extends React.Component {

  render() {
    return (
      <div className="steps">
        <div className="steps__title">
            <img className="steps__img" src="./img/logo2.png" alt=""/>
            <h2 className="title">Magister</h2>
        </div>

        <ul className="steps-list">
            <li className="step">¿En qué te quieres especializar?</li>
            <li className="step">Horario y Modalidad</li>
            <li className="step">Tarifa</li>
            <li className="step">Datos Personales</li>
            <li className="step">Dirección</li>
            <li className="step">Forma de pago</li>
        </ul>

        <img className="fondo-steps" src="./img/fondo1.png" alt="" />
      </div>
    );
  }
}

export default Steps;