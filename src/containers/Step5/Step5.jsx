import React from 'react';
import { useDispatch } from 'react-redux';
import {setComunidad} from "../../app/appSlice";
import './Step5.scss';
import {Link} from 'react-router-dom';
import Steps from '../Steps';

const Step5 = props => {

  const dispatch=useDispatch();
  const { comunidadesList } = props;

  console.log('props redux: ', comunidadesList);

  const handleChangeComunidad = (ev) => {
      dispatch(setComunidad(ev.target.value));
  };

    return (
      <div className="step5">
        <Steps />
        <div className="step-5">
          <h1 className="step-5__title">Tu dirección</h1>

          <div className="direction-box1">
            <h3 className="direction__title">Comunidad de Exámen</h3>
            <p className="direction__description">(Selecciona una opción)</p>
            
            <form className="direction__form">

                { comunidadesList ? (

                  <select className="direction__select" onChange={handleChangeComunidad}>
                  { comunidadesList.map(comunidad => {
                  return(
                    <option className="direction__option" key={comunidad} value={comunidad}>{comunidad}</option>
                  )
                  })}
                  </select>

                ): null}

            </form>
          </div>

          <div className="direction__box2">
            <p className="direction__name">Dirección</p>
            <form className="direction__form">
                <input
                type="text" 
                placeholder="Ej: Calle Ramiro de Maxtu, 10, 4D" 
                className="direction__input"/>
            </form>
          </div>

          <div className="direction__box3">
            <div className="direction__box">
                <p className="direction__name">Localidad</p>
                <form className="direction__form">
                    <input
                    type="text" 
                    placeholder="Ej: Madrid" 
                    className="direction__input2"/>
                </form>
            </div>

            <div className="direction__box">
                <p className="direction__name">Provincia</p>
                <form className="direction__form">
                    <input
                    type="text" 
                    placeholder="Ej: Madrid" 
                    className="direction__input2"/>
                </form>
            </div>

            <div className="direction__box">
                <p className="direction__name">Código Postal</p>
                <form className="direction__form">
                    <input
                    type="text" 
                    placeholder="Ej: 28040" 
                    className="direction__input2"/>
                </form>
            </div>
          </div>

            <div className="step-buttons">
              <button className="button-1"><Link className="link" to="/step6">Siguiente</Link></button>
              <button className="button-2"><Link className="link-two" to="/step4">Atrás</Link></button>
            </div>
        </div>
      </div>
    );
  }

export default Step5;