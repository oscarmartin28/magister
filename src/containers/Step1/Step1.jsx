import React from 'react';
import {Link} from 'react-router-dom';
import { useDispatch } from 'react-redux';
import {setProvincia, setRama} from "../../app/appSlice";
import Steps from '../Steps';
import './Step1.scss';

const Step1 = props => {
    const dispatch=useDispatch();
    const { ramasList, provinciasList } = props;

    console.log('props redux: ',ramasList, provinciasList)

    const handleChangeRama = (ev) => {
        dispatch(setRama(ev.target.value));
    };

    const handleChangeProvincia = (ev) => {
        dispatch(setProvincia(ev.target.value));
    };

    return (
      <div className="step1">
        <Steps />
        <div className="step-1">
          <h1 className="step-1__title">¿En qué te quieres especializar?</h1>
          <div className="rama-provincia">
            <div className="rama">
              <h3 className="rama__title">Rama</h3>
              <p className="rama__description">(Selecciona una opción)</p>
              <form className="rama__form">
                   
                { ramasList ? (

                  <select className="form__select" onChange={handleChangeRama}>
                  { ramasList.map(rama => {
                  return(
                    <option
                        className="form__option"
                        key={rama}
                        value={rama}>
                        {rama}
                    </option>
                  )
                  })}
                  </select>

                ): null}
                   
              </form>
            </div>

            <div className="provincia">
              <h3 className="provincia__title">Provincia</h3>
              <p className="provincia__description">(Selecciona una opción)</p>
              <form className="provincia__form">
                
                { provinciasList ? (

                  <select className="form__select" onChange={handleChangeProvincia}>
                  { provinciasList.map(provincia => {
                  return(
                    <option className="form__option" key={provincia} value={provincia}>{provincia}</option>
                  )
                  })}
                  </select>

                ): null}

              </form>
            </div>
          </div>

          <div className="alumno">
            <p className="alumno__description">¿Has sido alumn@ de Magister?</p>
            <p className="alumno__consulta">Consulta condiciones</p>

            <div className="alumno__buttons">
              <button className="alumno__button">No</button>
              <button className="alumno__button">Si</button>
              <button className="alumno__button button__three">Si, después de 2017</button>
            </div>
          </div>

          <div className="alumno">
            <p className="alumno__description">Entrega de material</p>
            <p className="alumno__consulta">Consulta condiciones</p>

            <div className="alumno__buttons">
              <button className="alumno__button button__four">Material mes a mes</button>
            </div>
          </div>

          <div className="step-buttons">
              <button className="button-1"><Link className="link" to="/step2">Siguiente</Link></button>
          </div>
        </div>
      </div>
    );
}

export default Step1;