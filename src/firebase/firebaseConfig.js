import firebase from 'firebase/app';
import '@firebase/firestore';

var firebaseConfig = {
    apiKey: "AIzaSyBJHeChl0Lzpvl3yQbjhMuPC1kkyanDD1Q",
    authDomain: "magister-29963.firebaseapp.com",
    projectId: "magister-29963",
    storageBucket: "magister-29963.appspot.com",
    messagingSenderId: "14375702609",
    appId: "1:14375702609:web:725a2a2cfb28e68f532870",
    measurementId: "G-RNK61FWX3K"
};

firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore();